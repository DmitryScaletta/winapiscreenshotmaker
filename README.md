# WinAPI Screenshot Maker
Курсовая по ОСиСП  
Тема: Разработка программы для создания скриншотов

# Библиотеки
SimpleIni - https://github.com/brofield/simpleini

# Скачать .exe
https://bitbucket.org/DmitryScaletta/winapiscreenshotmaker/downloads

# Visual C++ Redistributable
https://www.microsoft.com/ru-ru/download/confirmation.aspx?id=48145