#pragma once

enum { INTO_CLIPBOARD = 0, INTO_FILE };
enum { FORMAT_BMP = 0, FORMAT_PNG, FORMAT_JPG };

bool screenshot(const WCHAR * filename = L"", const bool into_file = 0, const int file_format = 0);