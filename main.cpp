﻿#include <Windows.h>
#include <ShlObj.h>
#include <string>
#include <vector>
#include <iomanip> // std::setw() std::setfill()
#include <sstream> // std::wstringstream
#include <cwctype> // std::iswspace()
#include "SimpleIni.h"
#include "resource.h"

#include "engine.h"

#pragma comment(linker, \
  "\"/manifestdependency:type='Win32' "\
  "name='Microsoft.Windows.Common-Controls' "\
  "version='6.0.0.0' "\
  "processorArchitecture='*' "\
  "publicKeyToken='6595b64144ccf1df' "\
  "language='*'\"")

#pragma comment(lib, "ComCtl32.lib")
#pragma comment(lib, "Winmm.lib")




/* ============================================================ */
/* global variables */
/* ============================================================ */

enum { 
	S_SAVE_INTO_FILE = 0, 
	S_FILE_FORMAT, 
	S_OUTPUT_FOLDER, 
	S_NAME, 
	S_BUTTON, 
	S_SOUND
};
enum { NAME_MAX_LENGTH = 80 };
enum { SETTINGS_COUNT = 6 };

std::vector<std::wstring> settings(SETTINGS_COUNT);

const bool IS_UTF8 = true;

const WCHAR * SETTINGS_FILE_NAME    = L"settings.ini";

const WCHAR * SECTION_NAME          = L"Global";

const WCHAR * INI_SAVE_INTO_FILE    = L"SaveIntoFile";
const WCHAR * INI_FILE_FORMAT       = L"FileFormat";
const WCHAR * INI_OUTPUT_FOLDER     = L"OutputFolder";
const WCHAR * INI_SCREENSHOT_NAME   = L"ScreenshotName";
const WCHAR * INI_SCREENSHOT_BUTTON = L"ScreenshotButton";
const WCHAR * INI_SCREENSHOT_SOUND  = L"ScreenshotSound";

const WCHAR * DEFALUT_SAVE_TO_FILE      = L"0";
const WCHAR * DEFAULT_FILE_FORMAT       = L"BMP";
const WCHAR * DEFAULT_OUTPUT_FOLDER     = L"D:\\";
const WCHAR * DEFAULT_SCREENSHOT_NAME   = L"MyScreenshot";
const WCHAR * DEFAULT_SCREENSHOT_BUTTON = L"44";
const WCHAR * DEFAULT_SCREENSHOT_SOUND  = L"1";

const WCHAR * FORMAT_BMP_TEXT = L"BMP";
const WCHAR * FORMAT_PNG_TEXT = L"PNG";
const WCHAR * FORMAT_JPG_TEXT = L"JPG";

const WCHAR * EXTENSION_BMP   = L".bmp";
const WCHAR * EXTENSION_PNG   = L".png";
const WCHAR * EXTENSION_JPG   = L".jpg";

const WCHAR * FALSE_TEXT = L"0";
const WCHAR * TRUE_TEXT  = L"1";

const WCHAR * F1_TEXT  = L"F1";
const WCHAR * F2_TEXT  = L"F2";
const WCHAR * F3_TEXT  = L"F3";
const WCHAR * F4_TEXT  = L"F4";
const WCHAR * F5_TEXT  = L"F5";
const WCHAR * F6_TEXT  = L"F6";
const WCHAR * F7_TEXT  = L"F7";
const WCHAR * F8_TEXT  = L"F8";
const WCHAR * F9_TEXT  = L"F9";
const WCHAR * F10_TEXT = L"F10";
const WCHAR * F11_TEXT = L"F11";
const WCHAR * PRINT_SCREEN_TEXT = L"Print Screen";

enum {
	BUTTON_F1  = VK_F1,
	BUTTON_F2  = VK_F2,
	BUTTON_F3  = VK_F3,
	BUTTON_F4  = VK_F4,
	BUTTON_F5  = VK_F5,
	BUTTON_F6  = VK_F6,
	BUTTON_F7  = VK_F7,
	BUTTON_F8  = VK_F8,
	BUTTON_F9  = VK_F9,
	BUTTON_F10 = VK_F10,
	BUTTON_F11 = VK_F11,
	BUTTON_PRINT_SCREEN = VK_SNAPSHOT
};

enum { MAKE_SCREENSHOT_BUTTON };



/* ============================================================ */
/* lables */
/* ============================================================ */

const WCHAR * LBL_MAIN_WINDOW_NAME      = L"WinAPI Screenshot Maker";

const WCHAR * LBL_STATIC1               = L"Вывод скриншота";
const WCHAR * LBL_STATIC2               = L"Формат файла";
const WCHAR * LBL_STATIC3               = L"Папка для сохранения скриншотов";
const WCHAR * LBL_STATIC4               = L"Имя скриншота";
const WCHAR * LBL_STATIC5               = L"Кнопка для создания скриншота";

const WCHAR * LBL_INTO_CLIPBOARD        = L"В буфер обмена";
const WCHAR * LBL_INTO_FILE             = L"В файл";

const WCHAR * LBL_CHANGE_OUTPUT_FOLDER  = L"Обзор...";
const WCHAR * LBL_SCREENSHOT_SOUND      = L"Воспроизводить звук при создании скриншота";

const WCHAR * LBL_SAVE_SETTINGS         = L"Сохранить настройки";
const WCHAR * LBL_EXIT                  = L"Выход";

const WCHAR * LBL_SETTINGS_SAVED        = L"Настройки сохранены";
const WCHAR * LBL_SETTINGS_SAVED_TITLE  = L"Настройки";

const WCHAR * LBL_EXIT_QUESTION         = L"Вы действительно хотите закрыть программу?";
const WCHAR * LBL_EXIT_QUESTION_TITLE   = L"Выход";

const WCHAR * LBL_SELECT_FOLDER         = L"Выберите папку для сохранения скриншотов";



/* ============================================================ */
/* settings */
/* ============================================================ */

bool read_settings(std::vector<std::wstring> & settings)
{
	// load from a data file
	CSimpleIniW ini(IS_UTF8);
	SI_Error rc = ini.LoadFile(SETTINGS_FILE_NAME);

	// read data
	settings[S_SAVE_INTO_FILE] = ini.GetValue(SECTION_NAME, INI_SAVE_INTO_FILE,    DEFALUT_SAVE_TO_FILE);
	settings[S_FILE_FORMAT]    = ini.GetValue(SECTION_NAME, INI_FILE_FORMAT,       DEFAULT_FILE_FORMAT);
	settings[S_OUTPUT_FOLDER]  = ini.GetValue(SECTION_NAME, INI_OUTPUT_FOLDER,     DEFAULT_OUTPUT_FOLDER);
	settings[S_NAME]           = ini.GetValue(SECTION_NAME, INI_SCREENSHOT_NAME,   DEFAULT_SCREENSHOT_NAME);
	settings[S_BUTTON]         = ini.GetValue(SECTION_NAME, INI_SCREENSHOT_BUTTON, DEFAULT_SCREENSHOT_BUTTON);
	settings[S_SOUND]          = ini.GetValue(SECTION_NAME, INI_SCREENSHOT_SOUND,  DEFAULT_SCREENSHOT_SOUND);

	return true;
}

bool write_settings(const std::vector<std::wstring> settings)
{
	CSimpleIniW ini(IS_UTF8);

	if (ini.SetValue(SECTION_NAME, INI_SAVE_INTO_FILE,    settings[S_SAVE_INTO_FILE].c_str()) < 0) return false;
	if (ini.SetValue(SECTION_NAME, INI_FILE_FORMAT,       settings[S_FILE_FORMAT].c_str())    < 0) return false;
	if (ini.SetValue(SECTION_NAME, INI_OUTPUT_FOLDER,     settings[S_OUTPUT_FOLDER].c_str())  < 0) return false;
	if (ini.SetValue(SECTION_NAME, INI_SCREENSHOT_NAME,   settings[S_NAME].c_str())           < 0) return false;
	if (ini.SetValue(SECTION_NAME, INI_SCREENSHOT_BUTTON, settings[S_BUTTON].c_str())         < 0) return false;
	if (ini.SetValue(SECTION_NAME, INI_SCREENSHOT_SOUND,  settings[S_SOUND].c_str())          < 0) return false;

	// save the data back to the file
	SI_Error rc = ini.SaveFile(SETTINGS_FILE_NAME);
	if (rc < 0) return false;

	return true;
}



/* ============================================================ */
/* strings */
/* ============================================================ */

std::wstring & trim(std::wstring & str)
{
	if (str.empty()) return str;
	while (std::iswspace(str[0])) str.erase(0, 1);
	while (std::iswspace(str[str.length() - 1])) str.erase(str.length() - 1, 1);
	return str;
}

std::wstring & remove_illegal_chars(std::wstring & s)
{
	std::wstring illegal_chars = L"\\/:*?\"<>|";
	std::wstring::iterator it;
	for (it = s.begin(); it < s.end(); ++it)
	{
		bool found = illegal_chars.find(*it) != std::wstring::npos;
		if (found) { *it = ' '; }
	}
	return s;
}

std::wstring current_date_and_time()
{
	SYSTEMTIME time;
	GetSystemTime(&time);
	
	std::wstringstream res;
 
    res << time.wYear << L"."
        << std::setw(2) << std::setfill(L'0') << time.wMonth  << L"."
        << std::setw(2) << std::setfill(L'0') << time.wDay    << L" "
        << std::setw(2) << std::setfill(L'0') << time.wHour   << L"."
        << std::setw(2) << std::setfill(L'0') << time.wMinute << L"."
        << std::setw(2) << std::setfill(L'0') << time.wSecond << L"."
        << std::setw(3) << std::setfill(L'0') << time.wMilliseconds;

	return res.str();
}



/* ============================================================ */
/* file system */
/* ============================================================ */

bool directory_exist(const WCHAR * path)
{
	DWORD ftyp = GetFileAttributes(path);
	if (ftyp == INVALID_FILE_ATTRIBUTES) return false;  // something is wrong with your path!
	if (ftyp & FILE_ATTRIBUTE_DIRECTORY) return true;   // this is a directory!
	return false;    // this is not a directory!
}



/* ============================================================ */
/* actions */
/* ============================================================ */

void bind_global_hotkey(HWND hDlg)
{
	UnregisterHotKey(hDlg, MAKE_SCREENSHOT_BUTTON);

	int key;
	try { key = std::stoi(settings[S_BUTTON]); }
	catch (...) 
	{
		key = std::stoi(DEFAULT_SCREENSHOT_BUTTON);
		settings[S_BUTTON] = DEFAULT_SCREENSHOT_BUTTON;
	}

	switch (key)
	{
	case BUTTON_F1:
	case BUTTON_F2:
	case BUTTON_F3:
	case BUTTON_F4:
	case BUTTON_F5:
	case BUTTON_F6:
	case BUTTON_F7:
	case BUTTON_F8:
	case BUTTON_F9:
	case BUTTON_F10:
	case BUTTON_F11:
	case BUTTON_PRINT_SCREEN:
		RegisterHotKey(hDlg, MAKE_SCREENSHOT_BUTTON, MOD_NOREPEAT, key);
		break;
	
	default:
		RegisterHotKey(hDlg, MAKE_SCREENSHOT_BUTTON, MOD_NOREPEAT, std::stoi(DEFAULT_SCREENSHOT_BUTTON));
	}
}

void update_dialog(HWND hDlg)
{
	if (IsDlgButtonChecked(hDlg, IDC_INTO_FILE))
	{
		EnableWindow(GetDlgItem(hDlg, IDC_FORMAT_BMP),           TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_FORMAT_PNG),           TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_FORMAT_JPG),           TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_OUTPUT_FOLDER),        TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SCREENSHOT_NAME),      TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHANGE_OUTPUT_FOLDER), TRUE);
	}
	else
	{
		EnableWindow(GetDlgItem(hDlg, IDC_FORMAT_BMP),           FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_FORMAT_PNG),           FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_FORMAT_JPG),           FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_OUTPUT_FOLDER),        FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SCREENSHOT_NAME),      FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHANGE_OUTPUT_FOLDER), FALSE);
	}
}

void on_init_dialog(HWND hDlg)
{
	// set captions
	SetWindowText(hDlg,                            LBL_MAIN_WINDOW_NAME);

	SetDlgItemText(hDlg, IDC_STATIC1,              LBL_STATIC1);
	SetDlgItemText(hDlg, IDC_STATIC2,              LBL_STATIC2);
	SetDlgItemText(hDlg, IDC_STATIC3,              LBL_STATIC3);
	SetDlgItemText(hDlg, IDC_STATIC4,              LBL_STATIC4);
	SetDlgItemText(hDlg, IDC_STATIC5,              LBL_STATIC5);

	SetDlgItemText(hDlg, IDC_INTO_CLIPBOARD,       LBL_INTO_CLIPBOARD);
	SetDlgItemText(hDlg, IDC_INTO_FILE,            LBL_INTO_FILE);

	SetDlgItemText(hDlg, IDC_FORMAT_BMP,           FORMAT_BMP_TEXT);
	SetDlgItemText(hDlg, IDC_FORMAT_PNG,           FORMAT_PNG_TEXT);
	SetDlgItemText(hDlg, IDC_FORMAT_JPG,           FORMAT_JPG_TEXT);

	SetDlgItemText(hDlg, IDC_CHANGE_OUTPUT_FOLDER, LBL_CHANGE_OUTPUT_FOLDER);
	SetDlgItemText(hDlg, IDC_SCREENSHOT_SOUND,     LBL_SCREENSHOT_SOUND);

	SetDlgItemText(hDlg, IDC_SAVE_SETTINGS,        LBL_SAVE_SETTINGS);
	SetDlgItemText(hDlg, IDC_EXIT,                 LBL_EXIT);


	// read options
	read_settings(settings);


	// into clipboard or into file
	if (settings[S_SAVE_INTO_FILE] == TRUE_TEXT)    { SendDlgItemMessage(hDlg, IDC_INTO_FILE,      BM_SETCHECK, TRUE, 0); }
	else                                            { SendDlgItemMessage(hDlg, IDC_INTO_CLIPBOARD, BM_SETCHECK, TRUE, 0); }


	// file format
	if      (settings[S_FILE_FORMAT] == FORMAT_BMP_TEXT)    { SendDlgItemMessage(hDlg, IDC_FORMAT_BMP, BM_SETCHECK, TRUE, 0); } 
	else if (settings[S_FILE_FORMAT] == FORMAT_PNG_TEXT)    { SendDlgItemMessage(hDlg, IDC_FORMAT_PNG, BM_SETCHECK, TRUE, 0); }
	else if (settings[S_FILE_FORMAT] == FORMAT_JPG_TEXT)    { SendDlgItemMessage(hDlg, IDC_FORMAT_JPG, BM_SETCHECK, TRUE, 0); }
	else                                                    { SendDlgItemMessage(hDlg, IDC_FORMAT_BMP, BM_SETCHECK, TRUE, 0); } // default


	// output folder
	if (settings[S_OUTPUT_FOLDER].empty()) settings[S_OUTPUT_FOLDER] = DEFAULT_OUTPUT_FOLDER;
	
	if (!directory_exist(settings[S_OUTPUT_FOLDER].c_str())) { settings[S_OUTPUT_FOLDER] = DEFAULT_OUTPUT_FOLDER; }

	SetDlgItemText(hDlg, IDC_OUTPUT_FOLDER, settings[S_OUTPUT_FOLDER].c_str());


	// screenshot name
	if (settings[S_NAME].empty()) settings[S_NAME] = DEFAULT_SCREENSHOT_NAME;
	remove_illegal_chars(settings[S_NAME]);
	trim(settings[S_NAME]);

	if (settings[S_NAME].empty()) { settings[S_NAME] = DEFAULT_SCREENSHOT_NAME; }

	SetDlgItemText(hDlg, IDC_SCREENSHOT_NAME, settings[S_NAME].c_str());


	// screenshot button
	SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_ADDSTRING, NULL, (LPARAM)F1_TEXT);
	SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_ADDSTRING, NULL, (LPARAM)F2_TEXT);
	SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_ADDSTRING, NULL, (LPARAM)F3_TEXT);
	SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_ADDSTRING, NULL, (LPARAM)F4_TEXT);
	SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_ADDSTRING, NULL, (LPARAM)F5_TEXT);
	SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_ADDSTRING, NULL, (LPARAM)F6_TEXT);
	SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_ADDSTRING, NULL, (LPARAM)F7_TEXT);
	SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_ADDSTRING, NULL, (LPARAM)F8_TEXT);
	SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_ADDSTRING, NULL, (LPARAM)F9_TEXT);
	SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_ADDSTRING, NULL, (LPARAM)F10_TEXT);
	SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_ADDSTRING, NULL, (LPARAM)F11_TEXT);
	SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_ADDSTRING, NULL, (LPARAM)PRINT_SCREEN_TEXT);

	int key;
	try { key = std::stoi(settings[S_BUTTON]); }
	catch (...) 
	{
		key = std::stoi(DEFAULT_SCREENSHOT_BUTTON);
		settings[S_BUTTON] = DEFAULT_SCREENSHOT_BUTTON;
	}

	switch (key)
	{
	case BUTTON_F1:             SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_SETCURSEL, 0,  NULL); break;
	case BUTTON_F2:             SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_SETCURSEL, 1,  NULL); break;
	case BUTTON_F3:             SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_SETCURSEL, 2,  NULL); break;
	case BUTTON_F4:             SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_SETCURSEL, 3,  NULL); break;
	case BUTTON_F5:             SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_SETCURSEL, 4,  NULL); break;
	case BUTTON_F6:             SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_SETCURSEL, 5,  NULL); break;
	case BUTTON_F7:             SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_SETCURSEL, 6,  NULL); break;
	case BUTTON_F8:             SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_SETCURSEL, 7,  NULL); break;
	case BUTTON_F9:             SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_SETCURSEL, 8,  NULL); break;
	case BUTTON_F10:            SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_SETCURSEL, 9,  NULL); break;
	case BUTTON_F11:            SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_SETCURSEL, 10, NULL); break;
	case BUTTON_PRINT_SCREEN:   SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_SETCURSEL, 11, NULL); break;
	default:                    SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_SETCURSEL, 11, NULL); break;
	}


	// screenshot sound
	if (settings[S_SOUND] == TRUE_TEXT) { SendDlgItemMessage(hDlg, IDC_SCREENSHOT_SOUND, BM_SETCHECK, TRUE, 0); }

	update_dialog(hDlg);

	
	bind_global_hotkey(hDlg);
}

void save_settings(HWND hDlg)
{
	// into clipboard or into file
	if (IsDlgButtonChecked(hDlg, IDC_INTO_FILE)) { settings[S_SAVE_INTO_FILE] = TRUE_TEXT; }
	else                                         { settings[S_SAVE_INTO_FILE] = FALSE_TEXT; }


	// file format
	if      (IsDlgButtonChecked(hDlg, IDC_FORMAT_BMP)) { settings[S_FILE_FORMAT] = FORMAT_BMP_TEXT; }
	else if (IsDlgButtonChecked(hDlg, IDC_FORMAT_PNG)) { settings[S_FILE_FORMAT] = FORMAT_PNG_TEXT; }
	else if (IsDlgButtonChecked(hDlg, IDC_FORMAT_JPG)) { settings[S_FILE_FORMAT] = FORMAT_JPG_TEXT; }
	else                                               { settings[S_FILE_FORMAT] = FORMAT_BMP_TEXT; }


	// output folder
	WCHAR output_folder[MAX_PATH];
	GetDlgItemText(hDlg, IDC_OUTPUT_FOLDER, output_folder, MAX_PATH);
	if (!directory_exist(output_folder))
	{
		settings[S_OUTPUT_FOLDER] = DEFAULT_OUTPUT_FOLDER;
		SetDlgItemText(hDlg, IDC_OUTPUT_FOLDER, settings[S_OUTPUT_FOLDER].c_str());
	}
	else
	{
		settings[S_OUTPUT_FOLDER] = output_folder;
	}


	// screenshot name
	WCHAR name[NAME_MAX_LENGTH];
	GetDlgItemText(hDlg, IDC_SCREENSHOT_NAME, name, NAME_MAX_LENGTH);
	
	std::wstring wsname = name;
	remove_illegal_chars(wsname);
	trim(wsname);
	
	if (wsname.empty()) { settings[S_NAME] = DEFAULT_SCREENSHOT_NAME; }
	else                { settings[S_NAME] = wsname; }

	SetDlgItemText(hDlg, IDC_SCREENSHOT_NAME, settings[S_NAME].c_str());


	// screenshot button
	int index = SendDlgItemMessage(hDlg, IDC_SCREENSHOT_BUTTON, CB_GETCURSEL, NULL, NULL);

	switch (index)
	{
	case 0:  settings[S_BUTTON] = std::to_wstring(BUTTON_F1);           break;
	case 1:  settings[S_BUTTON] = std::to_wstring(BUTTON_F2);           break;
	case 2:  settings[S_BUTTON] = std::to_wstring(BUTTON_F3);           break;
	case 3:  settings[S_BUTTON] = std::to_wstring(BUTTON_F6);           break;
	case 4:  settings[S_BUTTON] = std::to_wstring(BUTTON_F5);           break;
	case 5:  settings[S_BUTTON] = std::to_wstring(BUTTON_F6);           break;
	case 6:  settings[S_BUTTON] = std::to_wstring(BUTTON_F7);           break;
	case 7:  settings[S_BUTTON] = std::to_wstring(BUTTON_F8);           break;
	case 8:  settings[S_BUTTON] = std::to_wstring(BUTTON_F9);           break;
	case 9:  settings[S_BUTTON] = std::to_wstring(BUTTON_F10);          break;
	case 10: settings[S_BUTTON] = std::to_wstring(BUTTON_F11);          break;
	case 12: settings[S_BUTTON] = std::to_wstring(BUTTON_PRINT_SCREEN); break;
	default: settings[S_BUTTON] = std::to_wstring(BUTTON_PRINT_SCREEN); break;
	}


	// screenshot sound
	if (IsDlgButtonChecked(hDlg, IDC_SCREENSHOT_SOUND)) { settings[S_SOUND] = TRUE_TEXT; }
	else                                                { settings[S_SOUND] = FALSE_TEXT; }


	write_settings(settings);


	bind_global_hotkey(hDlg);
}

void on_save_settings(HWND hDlg)
{
	save_settings(hDlg);

	MessageBox(hDlg, LBL_SETTINGS_SAVED, LBL_SETTINGS_SAVED_TITLE, NULL);
}

void on_close(HWND hDlg)
{
	if (MessageBox(hDlg, LBL_EXIT_QUESTION, LBL_EXIT_QUESTION_TITLE, MB_ICONQUESTION | MB_YESNO) == IDYES)
	{
		save_settings(hDlg);
		DestroyWindow(hDlg);
	}
}

void on_change_output_folder(HWND hDlg)
{
	TCHAR szDir[MAX_PATH];
	BROWSEINFO bInfo;
	bInfo.hwndOwner = hDlg;
	bInfo.pidlRoot = NULL;
	bInfo.pszDisplayName = szDir;        // address of a buffer to receive the display name of the folder selected by the user
	bInfo.lpszTitle = LBL_SELECT_FOLDER; // title of the dialog
	bInfo.ulFlags = 0;
	bInfo.lpfn = NULL;
	bInfo.lParam = 0;
	bInfo.iImage = -1;

	LPITEMIDLIST lpItem = SHBrowseForFolder(&bInfo);
	if (lpItem != NULL)
	{
		SHGetPathFromIDList(lpItem, szDir);
		SetDlgItemText(hDlg, IDC_OUTPUT_FOLDER, szDir);
	}
}

void make_screenshot()
{
	if (settings[S_SAVE_INTO_FILE] == TRUE_TEXT)
	{
		// full image path
		std::wstring filename;

		if (!directory_exist(settings[S_OUTPUT_FOLDER].c_str())) { settings[S_OUTPUT_FOLDER] = DEFAULT_OUTPUT_FOLDER; }

		remove_illegal_chars(settings[S_NAME]);
		trim(settings[S_NAME]);

		if (settings[S_NAME].empty()) settings[S_NAME] = DEFAULT_SCREENSHOT_NAME;

		filename = settings[S_OUTPUT_FOLDER];
		
		if (filename[filename.size() - 1] != L'/' || filename[filename.size() - 1] != L'\\') { filename += L"\\"; }
		
		filename += settings[S_NAME] + L" " + current_date_and_time();


		// make screenshot
		if (settings[S_FILE_FORMAT] == FORMAT_PNG_TEXT) 
		{
			filename += EXTENSION_PNG;
			screenshot(filename.c_str(), INTO_FILE, FORMAT_PNG);
		}
		else if (settings[S_FILE_FORMAT] == FORMAT_JPG_TEXT) 
		{
			filename += EXTENSION_JPG;
			screenshot(filename.c_str(), INTO_FILE, FORMAT_JPG);
		}
		else
		{
			filename += EXTENSION_BMP;
			screenshot(filename.c_str(), INTO_FILE, FORMAT_BMP);
		}
	}
	else
	{
		// into clipboard
		screenshot();
	}

	if (settings[S_SOUND] == TRUE_TEXT)
	{
		PlaySound(MAKEINTRESOURCE(IDR_WAVE1), GetModuleHandle(NULL), SND_ASYNC | SND_RESOURCE);

	}
}



/* ============================================================ */
/* dialog procedure */
/* ============================================================ */

INT_PTR CALLBACK DialogProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_EXIT:                  SendMessage(hDlg, WM_CLOSE, 0, 0);  return TRUE;
		case IDC_SAVE_SETTINGS:         on_save_settings(hDlg);             return TRUE;

		case IDC_INTO_CLIPBOARD:        update_dialog(hDlg);                return TRUE;
		case IDC_INTO_FILE:             update_dialog(hDlg);                return TRUE;

		case IDC_CHANGE_OUTPUT_FOLDER:	on_change_output_folder(hDlg);	    return TRUE;
		case IDC_SCREENSHOT_BUTTON:     bind_global_hotkey(hDlg);           return TRUE;
		}
		break;

	case WM_INITDIALOG: on_init_dialog(hDlg);   return TRUE;
	case WM_CLOSE:      on_close(hDlg);         return TRUE;

	case WM_DESTROY:    PostQuitMessage(0);     return TRUE;
	}

	return FALSE;
}



/* ============================================================ */
/* WinMain */
/* ============================================================ */

int WINAPI wWinMain(HINSTANCE hInst, HINSTANCE h0, LPTSTR lpCmdLine, int nCmdShow)
{
	HWND hDlg;
	MSG msg;
	BOOL ret;

	InitCommonControls();
	hDlg = CreateDialogParam(hInst, MAKEINTRESOURCE(IDD_MAIN), 0, DialogProc, 0);
	ShowWindow(hDlg, nCmdShow);

	while ((ret = GetMessage(&msg, 0, 0, 0)) != 0) 
	{
		if (ret == -1)
			return -1;

		if (!IsDialogMessage(hDlg, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		switch (msg.message)
		{
		case WM_HOTKEY: if (msg.wParam == MAKE_SCREENSHOT_BUTTON) { make_screenshot(); } break;
		}
	}

	return 0;
}