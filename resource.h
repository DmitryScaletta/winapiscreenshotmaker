//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Resource.rc
//
#define IDD_MAIN                        101
#define IDR_WAVE1                       103
#define IDC_OUTPUT_FOLDER               1001
#define IDC_CHANGE_OUTPUT_FOLDER        1002
#define IDC_SCREENSHOT_SOUND            1003
#define IDC_SCREENSHOT_NAME             1004
#define IDC_INTO_CLIPBOARD              1005
#define IDC_INTO_FILE                   1006
#define IDC_FORMAT_BMP                  1007
#define IDC_FORMAT_PNG                  1008
#define IDC_FORMAT_JPG                  1009
#define IDC_SAVE_SETTINGS               1010
#define IDC_EXIT                        1011
#define IDC_SCREENSHOT_BUTTON           1012
#define IDC_STATIC1                     1014
#define IDC_STATIC2                     1015
#define IDC_STATIC3                     1016
#define IDC_STATIC4                     1017
#define IDC_STATIC5                     1018

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
